const mongoose = require ('mongoose');

const userSchema = new mongoose.Schema({


firstName: {
		type: String,
		required: [true, "firstName is required"]
	},
lastName: {
		type: String,
		required: [true, "lastName is required"]
	},
email: {
		type: String,
		required: [true, "email is required"]
	},
password: {
		type: String,
		required: [true, "password is required"]
	},
isActive: {
		type: Boolean,
		required: true,
	},
mobileNo: {
		type: String,
		required: [true, "Mobile Number is required"]
	},
enrollments: [
	{
		courseId: {
			type: String,
			required: [true, "courseId is required"]
		},
		description:{
			type: String,
			required: [true, "description is required"]
		},
		status: {
			type: String,
			default: "Enrolled"
			required:[true, "status is required"]
		},
		enrolledOn: {
			type: Date,
			default: new Date()
		},
	}
],

isAdmin: {
	type: Boolean,
	default: false,
}




})








module.exports = mongoose.model("User", userSchema);